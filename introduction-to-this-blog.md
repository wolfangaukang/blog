Introduction to this blog

This blog only came to existance because I was testing [this project](https://github.com/cfenollosa/bashblog).

So, right now I'm only testing a container with this program and seeing if it works as expected :). But I plan to post stuff here (or I hope so).

If you want to see the Dockerfile for this program, please check [here](https://gitlab.com/WolfangAukang/bashblog_dockerfile).

Tags: 2020, hello-world, introduction, first-post
