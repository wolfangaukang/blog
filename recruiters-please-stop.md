Recruiters, please stop

This is a post I wanted to make a long time ago since I changed my job modality (from working with a company to working as a consultant).

Before that change, I made sure I didn't want to show myself as available for work on LinkedIn. I even did that on Upwork, because in February I had moved into a new full time job and I didn't want to receive any contracts or requests, as I would definitely not be able to help anyone and I had a new reputation to build.

Now, forwarding to September, I am now constantly getting a lot of job position interview offers. I haven't changed my LinkedIn since February, and I will still say: I'm not available for work. I am already engaged with another company and I'm totally ensuring my dedication into working for that company, even if they try to boot me one day and I can't do anything (which is possible, as I'm a contractor right now). 

Today I watched a video from a Brazilian entrepreneur, and he mentions about this whole thing I'm talking about. Recruiters just see your profile, see your certifications and think "hey, I want that guy". They forget to see I'm currently working for a company, which is somewhat a very shitty move and a disconsideration for people that has lost their jobs in recent months and now are struggling to find something.

So please, you'd rather contact someone that really needs a job. I am not available. Thanks.

Tags: 2020, rant
